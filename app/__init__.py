from flask import Flask
from flask.ext.script import Manager


app=Flask(__name__)
manager=Manager(app)
app.secret_key='reality is broken'
app.config.from_object('config')

from app import views
