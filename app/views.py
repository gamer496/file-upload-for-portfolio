import os
from flask import Flask,request,redirect,url_for,render_template,jsonify
from werkzeug import secure_filename
from app import app

NOT_ALLOWED=set(['php'])

def is_allowed(filename):
    return '.' in filename and not  filename.rsplit('.',1) in NOT_ALLOWED

@app.route("/analyze",methods=["POST"])
def upload_file():
    print request
    k=request.files["file"]
    return jsonify({"size":len(k.read())})

@app.route("/")
@app.route("/index")
def index():
    return render_template("index.html")
